import { Fragment } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import style from '../styles/NavBar.module.css';

export default () => {
	return (
		<Fragment>
			<Navbar expand="lg" fixed="top" variant="light" className={style.navbar}>
				<Link href="/"><a className="navbar-brand">The VAULT-Ph</a></Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse className="basic-navbar-nav">
					<Nav className="ml-auto">
						<Link href="/register">
							<a className="nav-link">Register</a>
						</Link>
						<Link href="/login">
							<a className="nav-link">Log-in</a>
						</Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		</Fragment>
	)
}